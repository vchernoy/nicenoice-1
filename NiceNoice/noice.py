#0.61a devel

import os
import subprocess
from sys import platform
from time import sleep
import datetime

def NoiceBro(param):
    #initialize the command list
    command = []

    #a control constant for the iterator
    k = len(param['seq'])-1
    #a control variable for the iterator
    i = 0
    while i <= k :
        if i%param['farm'][1] == param['farm'][0]-1 :

            #format the output file name
            name = os.path.split(param['seq'][i])[1]
            output_frame = os.path.join(param['output_directory'],name)

            #get the starting index for temporal filtering
            t_range = param['command']['internal']['temporal_range']
            range_min = t_range*-1

            #make a list of frames to include, starting with the current frame
            framelist = ["-i", param['seq'][i]]
            while range_min <= t_range :
                position = min(max(i + range_min,0),k)
                #make sure the frame is in range and exclude the current frame from the list
                if (position > 0 and position < k) and range_min != 0 :
                    framelist += ["-i"]
                    framelist.append((param['seq'][position]))
                #accumulate the iterator
                range_min+=1

            #include the input and output frames with the parsed command
            command = param['command']['external'] + framelist + ["-o",output_frame]
            print ""
            print "--------------------------------------------------------"
            print datetime.datetime.now()
            print ""
            print param['seq'][i].split("/")[-1]
            print ""
            print "Starting Arnold Denoiser with the following subprocess:"
            print command

            #subprocess noice if the dry run parameter is false
            #ignoring verbosity flag until better solution is found
            if param['dry'] == False :
                noice = subprocess.Popen(command, stdout=subprocess.PIPE)
                if param['noicey'] == False :
                    while True:
                        line = noice.stdout.readline()
                        if not line:
                            print ""
                            if os.path.exists(output_frame) :
                                print "Frame denoised without exception. Tah Dah!"
                            else :
                                print "**********************************************************************"
                                print "ERROR: " + param['seq'][i].split("/")[-1] + " WAS UNSUCCESSFUL! Try diagnosing with -noicey"
                                print "**********************************************************************"
                            break
                else :
                    if param['noicey'] == True :
                        output = noice.communicate()[0]
                        print output

        #accumulate the iterator
        i+=1

    print "All frames denoised."

def parse(instructions):
    #open the instructions
    instruction_file = open(instructions,'r')
    #save the unparsed instructions to a list
    unparsed = instruction_file.read().replace("\n","").split(";")
    #close the file
    instruction_file.close()
    #get rid of the unoccupied elements
    unparsed = filter(None, unparsed)
    #initialize dictionary
    parsed = {}
    #external commands formatted directly for subprocess
    parsed['external'] = []
    #a dictionary for internal commands for further use elsewhere
    parsed['internal'] = {}
    #parsing
    for item in unparsed:
        #split up the flags into lists using "=" as the key
        flag = item.split("=")
        #look for internal commands to be excluded from formatted list
        if flag[0] == "temporal_range" :
            parsed['internal'][flag[0]] = int(flag[1])
        #place the executable first
        elif flag[0] == "exe":
            if platform == "win32" :
                parsed['external'].append(flag[1])
                "Using 'exe' instruction: " + flag[1]
                if not os.path.isfile(flag[1]) :
                    print "Noice exe does not point to Noice. This will cause an error..."
                    return
                else:
                    print "Using 'exe' instruction: " + flag[1]
            else :
                pass
        elif flag[0] == "app":
            if platform == "darwin" :
                parsed['external'].append(flag[1])
                if not os.path.isfile(flag[1]) :
                    print "Noice app does not point to Noice. This will cause an error..."
                    return
                else:
                    print "Using 'app' instruction: " + flag[1]
            else :
                pass
        #parse the rest of the instruction file
        else :
            parsed['external'].append("-"+flag[0])
            parsed['external'].append(flag[1])

    return parsed

def noice(param) :
    print "looking for frames in: " + param['directory']

    #initialize parameter dictionary
    param['output_directory'] = ""
    param['command'] = []
    param['seq'] = []

    #get a list of files
    for root, dirs, files in os.walk(param['directory'],topdown=False,  followlinks=False) : #topdown=False,followlinks=False):
        for name in files:
            the_path = os.path.join(root, name)
            the_file = name
            #include files only from the sequence directory
            if root == param['directory'] :
                #frames
                if the_file.split(".")[-1] == "exr" :
                    print "An exr file was found: " + the_file
                    param['seq'].append(the_path)
                #instructions
                elif the_file.split(".")[-1] == "instructions" :
                    if param['instructions'] == "" :
                        print "An instruction file was found: " + the_file
                        param['instructions'] = the_path
                    else :
                        print "an instruction file was found, but was superseded... Will wait 5 seconds."
                        sleep(5)
                        print "using the instructions:"
                        print param['instructions']

    #name the output directory
    param['output_directory'] = os.path.join(root,"denoised")

    #if no exrs are found
    if len(param['seq']) == 0 :
        print "no exr files found!"
        return
    #if no instructions are found
    if not os.path.isfile(param['instructions']) :
        print "no instructions were found!"
		#todo: ask to use a default
        return
    #look for and make the output directory when neccesary
    if os.path.exists(param['output_directory']) :
        print "output directory found"
    else :
        print "no output directory found ... making one now!"
        try :
            os.makedirs(param['output_directory'])
            print "output directory created."
        except :
            #barf
            print "output directory could not be made!"
            return
    #check the farm configuration
    if param['farm'][0]>param['farm'][1] :
        print "the machine is outside the farm! moof."
        return

    #read and parse the instruction file
    param['command'] = (parse(param['instructions']))
    #Finish the command formatting and run noice
    NoiceBro(param)
