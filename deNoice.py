#0.61a devel
import NiceNoice
import argparse


parser = argparse.ArgumentParser(description='Batch process frames through Arnold Denoiser using the NiceNoice module')

parser.add_argument('-path', metavar='--path', type=str, default="", help='the path where the sequence of frames is located')
parser.add_argument('-inst', metavar='--instructions', type=str, default="", help='the path where the sequence of frames is located')
parser.add_argument('-n', metavar='--node', type=int, default=1, help='The specific machine in a pool of machines, for frame distribution over multiple machines.')
parser.add_argument('-p', metavar='--pool', type=int, default=1, help='Total number of machines in the pool, for frame distribution over multiple machines.')
parser.add_argument('-noicey', action="store_true", help='Print back Noice output. This is not printed in real-time. May cause unpredictable failures.')
parser.add_argument('-dry', action="store_true", help='Do not subprocess Noice.')

args = parser.parse_args()

#initialize the param dictionary
param = {}
param['directory'] = ""

#the instructions file
param['instructions'] = args.inst
#distributed denoising parameters
#todo: make this a single argument. ie: param['farm'] = [pool.split("/")[0], pool.split("/")[0]]
param['farm'] = [args.n,args.p]
#verbosity
param['noicey'] = args.noicey

#ask for a path if there isn't one, otherwise set the parameter path
if args.path == "" :
    param['directory'] = raw_input("Where is the sequence?")
else:
    param['directory'] = args.path

#dry-run
param['dry'] = args.dry

#if a directory was specified, start the module
if param['directory'] != "" :
    NiceNoice.noice(param)
else:
    print "No Path Was Specified"
